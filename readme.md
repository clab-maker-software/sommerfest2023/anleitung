# Anleitung für alle Spiele

## Einleitung

Auf dem Sommerfest vom ClaB 2023 haben wir ein kleines Angebot "Spieleentwicklung in 10 Minuten" gemacht. Dabei wurden drei Spiele vorbereitet und mit den Teilnehmenden innerhalb von 10 Minuten nachentwickelt. 11 Spiele sind entstanden, die alle Varianten von den vorbereitet spielen sind. Hier in der Untergruppe Sommerfest2023 sind alle Spiele zu finden, die drei in der vorbereiteten Variante und die mit den Teilnehmenden entstandenen.

## Wie kann ich die Spiele spielen?

Die Spiele wurden alle auf eine Seite für sogenannten Indie game developer hochgeladen und sind dort direkt im Browser auf PC/Mac und teilweise Tablet und Smartphone spielbar. Das Spiel ClaB Jump funktioniert auf Tablet und Smartphone nicht so gut, man müsste einen Gamecontroller oder eine Tastatur verbinden. Dies sind die Links:

- [Spiel Blop Hop](https://tom-imk.itch.io/sommerfest-im-clab-spiel-blob-hop)
- [Spiel Balloon Plop](https://tom-imk.itch.io/sommerfest-im-clab-spiel-balloon-pop)
- [Spiel ClaB Jump](https://tom-imk.itch.io/sommerfest-im-clab-spiel-clab-jump)
- [Spiel #001](https://tom-imk.itch.io/sommerfest-im-clab-spiel-001)
- [Spiel #002](https://tom-imk.itch.io/sommerfest-im-clab-spiel-002)
- [Spiel #003](https://tom-imk.itch.io/sommerfest-im-clab-spiel-003)
- [Spiel #004](https://tom-imk.itch.io/sommerfest-im-clab-spiel-004)
- [Spiel #005](https://tom-imk.itch.io/sommerfest-im-clab-spiel-005)
- [Spiel #006](https://tom-imk.itch.io/sommerfest-im-clab-spiel-006)
- [Spiel #007](https://tom-imk.itch.io/sommerfest-im-clab-spiel-007)
- [Spiel #008](https://tom-imk.itch.io/sommerfest-im-clab-spiel-008)
- [Spiel #009](https://tom-imk.itch.io/sommerfest-im-clab-spiel-009)
- [Spiel #010](https://tom-imk.itch.io/sommerfest-im-clab-spiel-010)
- [Spiel #011](https://tom-imk.itch.io/sommerfest-im-clab-spiel-011)

## Wie kann ich die Spiele herunterladen und verändern?

Die Spiele wurden mit einer sogenannten Gameengine erstellt, in diesem Falle die frei verfügbare Gameengine Godot. Eine Gameengine vereinfacht die Entwicklung von spielen ernorm, weil sie viele Werkzeuge mitbringt um Spiele zu entwickeln. Godot ist nach Unreal Engine und Unity die drittgrößte Gameengine und wird immer besser und beliebter. Ein Spiel wird in Szenen aufgebaut, so haben wir z.B. oft eine extra Szene für die Spielfigur, eine Szene für die Spielwelt in welcher die Spielfigur platziert wird und Szenen für Objekte in der Spielwelt. Eine Szene ist aus mehreren verschiedenen Objekten in einer Art Baumstruktur aufgebaut. Die verschiedenen Teile haben unterschiedliche Aufgaben und geben den Objekten in der Spielwelt Eigenschaften und Verhalten. Dazu kann und muss man meist auch noch etwas programmieren. In Godot macht man das mit der Sprache GDScript. Die ist einfach zu erlernen und sehr nahe an einer anderen beliebten Programmiersprache orientiert und zwar Python. Godot ist gut für den Einstieg in Programmierung geeignet und weil Python so ähnlich ist, lohnt sich das Erlernen von GDScript für Leute die später "richtig" programmieren wollen. Natürlich ist Spieleentwicklung auch richtiges Programmieren. Alles was man braucht ist - Godot. Das kann man einfach herunterladen und installieren (PC & Mac) oder man versucht es mal mit Godot im Browser. Das ist noch nicht so weit entwickelt, funktioniert jedoch vielleicht auch auf dem Tablet. IhrMan braucht die neuere Version 4, die alte Version 3.5 funktioniert nicht mit den Spielen

- [Godot 4 herunterladen](https://godotengine.org)
- [Godot Engine im Browser](https://editor.godotengine.org)

Der deutsche YouTube Tom Bleek hat vor einiger Zeit eine gute Einführung in Godot in mehreren Teilen bereitgestellt. Die Einführung bezieht sich leider auf die alte Version 3, man kann aber alles auf die Version 4 übertragen. Außerdem arbeitet er mit ein paar anderen Leuten an einem echten Spiel für den Verkauf, komplett in Godot programmiert.

- [Einführung in Godot auf YouTube](https://www.youtube.com/playlist?list=PL1td_Fr5vMGOW0hasVEYlvfdm_oYh0xi9)

Wir finden dass man von YouTube-Videos am besten und schnellsten lernt mit Godot umzugehen. Da es leider anscheinden (noch) keine Videos zu Godot 4 auf deutsch gibt - wer Englisch kann ist klar im Vorteil - können wir hier keinen Link bereitstellen. Die deutsche Dokumentation von Godot ist auch noch auf dem Stand von der Version 3.

Die Spiele selbst sind hier in den einzelnen Projekten zu finden.

- [Spiele vom Sommerfest 2023](https://gitlab.com/clab-maker-software/sommerfest2023)

Alles was man tun muss ist eines der Projekte herunterladen und in Godot zu "importieren" und wenn alles hoffentlich erfolgreich geladen ist einfach F5 drücken und das Spiel startet. Was dann noch fehlt ist die Schriftart die wir verwendet haben. Die können wir nicht einfach hier mit reingeben, aber man kann sie kostenlos selbst herunterladen:

- [m5x7 Pixelschrift](https://managore.itch.io/m5x7)

Wenn ihr fragen habt, schreibt uns - die E-Mailadresse bitte zusammensetzen, wir möchten verhindern dass wir Spam bekommen:

thomas [punkt] lobig {ät} gmx (punkt) de